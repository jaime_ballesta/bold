package com.boldtest.weatherapp.presentation.search.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.boldtest.weatherapp.domain.Resource
import com.boldtest.weatherapp.domain.common.Errors.Companion.notFiltersErrorCode
import com.boldtest.weatherapp.domain.common.Errors.Companion.notLocationsErrorCode
import com.boldtest.weatherapp.domain.model.LocationItem
import com.boldtest.weatherapp.presentation.state.SearchState
import com.boldtest.weatherapp.usecases.GetLocationsByQueryUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SearchViewModel @Inject constructor(
    private val getLocationsByQueryUseCase: GetLocationsByQueryUseCase,
) : ViewModel() {

    private val _state = MutableStateFlow<SearchState>(SearchState.ShowLocations(emptyList()))
    val state: StateFlow<SearchState> = _state

    fun setSearchQuery(query: String) = viewModelScope.launch {
        if (query.isNotEmpty()) {
            getLocationsByQueryUseCase(query).collect { result ->
                when (result) {
                    is Resource.Error -> setErrorState(result.message)
                    is Resource.Loading -> setState(SearchState.LoadingState)
                    is Resource.Success -> result.data?.let { locations ->
                        setState(SearchState.ShowLocations(locations))
                        if (locations.isEmpty()) setErrorState(notFiltersErrorCode.toString())
                    } ?: setErrorState(notLocationsErrorCode.toString())
                }
            }
        } else setState(SearchState.ShowLocations(emptyList()))
    }

    fun locationSelected(locationItem: LocationItem) {
        setState(SearchState.GoToHome(locationItem.title))
    }

    private fun setErrorState(message: String?) {
        setState(SearchState.Error(message ?: "An unexpected error occured"))
    }

    private fun setState(state: SearchState) {
        _state.value = state
    }

}