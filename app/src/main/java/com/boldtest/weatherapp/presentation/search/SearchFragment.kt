package com.boldtest.weatherapp.presentation.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.appcompat.widget.Toolbar
import androidx.appcompat.widget.SearchView
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.boldtest.weatherapp.R
import com.boldtest.weatherapp.databinding.FragmentSearchBinding
import com.boldtest.weatherapp.domain.model.LocationItem
import com.boldtest.weatherapp.getErrorMessage
import com.boldtest.weatherapp.getResourceView
import com.boldtest.weatherapp.launchAndCollect
import com.boldtest.weatherapp.presentation.search.adapter.LocationAdapter
import com.boldtest.weatherapp.presentation.search.viewmodel.SearchViewModel
import com.boldtest.weatherapp.presentation.state.SearchState
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SearchFragment : Fragment() {
    private val searchViewModel: SearchViewModel by viewModels()
    private lateinit var _binding: FragmentSearchBinding
    private val binding get() = _binding
    private lateinit var adapter: LocationAdapter
    private lateinit var toolbar: Toolbar

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragmentSearchBinding.inflate(inflater, container, false)
        toolbar = getResourceView(R.id.toolbar) as Toolbar
        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = LocationAdapter(searchViewModel::locationSelected)
        collectViewModelFlows()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        toolbar.inflateMenu(R.menu.menu_search)
        toolbar.menu.findItem(R.id.search_bar_menu).apply {
            val searchView: SearchView = actionView as SearchView
            expandActionView()
            searchView.implementSearch()
        }
    }

    private fun collectViewModelFlows() =
        viewLifecycleOwner.launchAndCollect(searchViewModel.state) { result ->
            showLoading(false)
            when (result) {
                SearchState.LoadingState -> showLoading(true)
                is SearchState.GoToHome -> navigateToHome(result.titleLocation)
                is SearchState.Error -> errorHandler(result.errorMessage)
                is SearchState.ShowLocations -> loadLocations(result.weatherReport)
            }
        }

    private fun SearchView.implementSearch() {
        setBackgroundResource(R.drawable.search_background)
        queryHint = getString(R.string.query_hint)
        setOnCloseListener { findNavController().navigateUp() }
        setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?) = false

            override fun onQueryTextChange(newText: String?) = newText?.let {
                searchViewModel.setSearchQuery(newText)
                false
            } ?: false
        })
    }

    private fun loadLocations(locations: List<LocationItem>) = binding.apply {
        adapter.submitList(locations)
        citiesRecyclerView.adapter = adapter
    }

    private fun showLoading(isVisible: Boolean) {
        val progress = getResourceView(R.id.progress_view)
        progress.setBackgroundResource(android.R.color.transparent)
        progress.isVisible = isVisible
    }

    private fun showErrorDialog(description: String) {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle(R.string.titleError)
            .setMessage(description)
            .setPositiveButton(R.string.ok) { _, _ -> }
            .setNegativeButton(R.string.dismiss) { _, _ -> }.show()
    }

    private fun errorHandler(errorCode: String) {
        showErrorDialog(getErrorMessage(errorCode))
    }

    private fun navigateToHome(title: String) {
        val destination = SearchFragmentDirections.actionSearchFragmentToHomeFragment(title)
        findNavController().navigate(destination)
    }
}