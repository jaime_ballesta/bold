package com.boldtest.weatherapp.presentation.home

import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.MenuInflater
import androidx.fragment.app.Fragment
import androidx.appcompat.widget.Toolbar
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.boldtest.weatherapp.*
import com.boldtest.weatherapp.databinding.FragmentHomeBinding
import com.boldtest.weatherapp.domain.common.DataTypes
import com.boldtest.weatherapp.domain.model.ConsolidateWeatherItem
import com.boldtest.weatherapp.domain.model.WeatherDetailsItem
import com.boldtest.weatherapp.presentation.home.adapter.WeatherAdapter
import com.boldtest.weatherapp.presentation.home.viewmodel.WeatherViewModel
import com.boldtest.weatherapp.presentation.state.WeatherState
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : Fragment() {
    private val weatherViewModel: WeatherViewModel by viewModels()
    private lateinit var adapter: WeatherAdapter
    private lateinit var _binding: FragmentHomeBinding
    private val binding get() = _binding
    private lateinit var toolbar: Toolbar

    private val args: HomeFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        toolbar = getResourceView(R.id.toolbar) as Toolbar
        setHasOptionsMenu(true)
        onBackPressedCustomAction { requireActivity().finish() }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        collectViewModelFlows()
        with(weatherViewModel) {
            requestLocationPermission { permissionGranted ->
                if (permissionGranted || args.title.isNotBlank())
                    if (args.title.isNotBlank()) loadWeather(args.title) else init()
                else navigateToSearch()
            }
            adapter = WeatherAdapter(::weatherSelected)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        toolbar.inflateMenu(R.menu.menu_home)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.search_bar_menu -> navigateToSearch()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun collectViewModelFlows() =
        viewLifecycleOwner.launchAndCollect(weatherViewModel.state) { result ->
            showLoading(false)
            when (result) {
                WeatherState.GoToSearch -> navigateToSearch()
                WeatherState.LoadingState -> showLoading(true)
                is WeatherState.Error -> errorHandler(result.errorMessage)
                is WeatherState.ShowWeatherConsolidation -> loadConsolidation(result.weatherReport)
            }

        }

    private fun loadConsolidation(weatherReport: WeatherDetailsItem) = binding.apply {
        adapter.submitList(weatherReport.consolidatedWeather)
        daysRecyclerView.adapter = adapter
        cityTitle.text = buildTitle(weatherReport.title, weatherReport.parentItemModel.title)
        weatherSelected(weatherReport.consolidatedWeather.first())
    }

    private fun weatherSelected(weatherItem: ConsolidateWeatherItem) = binding.apply {
        with(weatherItem) {
            stateDescription.text = weatherStateName
            currentTemp.text = formatValue(theTemp, DataTypes.TEMPERATURE)
            minTempTextView.text = formatValue(minTemp, DataTypes.TEMPERATURE)
            maxTempTextView.text = formatValue(maxTemp, DataTypes.TEMPERATURE)
            windTextView.text = formatValue(windSpeed, DataTypes.SPEED)
            humidityTextView.text = formatValue(humidity, DataTypes.PERCENT)
            weatherStateImage.loadUrl(getString(R.string.buildUrlImage, weatherStateAbbr))
        }
    }

    private fun showLoading(isVisible: Boolean) {
        val progress = getResourceView(R.id.progress_view)
        progress.setBackgroundResource(R.color.white)
        progress.isVisible = isVisible
    }

    private fun showErrorDialog(description: String) {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle(R.string.titleError)
            .setMessage(description)
            .setPositiveButton(R.string.ok) { _, _ -> }
            .setNegativeButton(R.string.dismiss) { _, _ -> }.show()
    }

    private fun errorHandler(errorCode: String) {
        showErrorDialog(getErrorMessage(errorCode))
    }

    private fun navigateToSearch() {
        val destination = HomeFragmentDirections.actionHomeFragmentToSearchFragment()
        findNavController().navigate(destination)
    }
}