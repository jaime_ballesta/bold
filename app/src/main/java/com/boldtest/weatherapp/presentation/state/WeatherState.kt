package com.boldtest.weatherapp.presentation.state

import com.boldtest.weatherapp.domain.model.WeatherDetailsItem

sealed class WeatherState {
    object LoadingState : WeatherState()
    object GoToSearch: WeatherState()
    data class Error(val errorMessage: String) : WeatherState()
    data class ShowWeatherConsolidation(val weatherReport: WeatherDetailsItem): WeatherState()
}
