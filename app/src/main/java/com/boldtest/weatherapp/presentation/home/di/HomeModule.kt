package com.boldtest.weatherapp.presentation.home.di

import com.boldtest.weatherapp.data.repository.WeatherRepository
import com.boldtest.weatherapp.usecases.GetLocationsByCoordinatesUseCase
import com.boldtest.weatherapp.usecases.GetWeatherReportUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped

@Module
@InstallIn(ViewModelComponent::class)
class HomeModule {

    @Provides
    @ViewModelScoped
    fun provideGetByCoordinates(weatherRepository: WeatherRepository): GetLocationsByCoordinatesUseCase =
        GetLocationsByCoordinatesUseCase(weatherRepository)

    @Provides
    @ViewModelScoped
    fun provideGetWeatherReport(weatherRepository: WeatherRepository): GetWeatherReportUseCase =
        GetWeatherReportUseCase(weatherRepository)
}