package com.boldtest.weatherapp.presentation.home.adapter

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.boldtest.weatherapp.inflate
import com.boldtest.weatherapp.loadUrl
import com.boldtest.weatherapp.formatValue
import com.boldtest.weatherapp.R
import com.boldtest.weatherapp.databinding.ItemViewWeatherDayBinding
import com.boldtest.weatherapp.domain.common.DataTypes
import com.boldtest.weatherapp.domain.model.ConsolidateWeatherItem

class WeatherAdapter(private val listener: (ConsolidateWeatherItem) -> Unit) :
    ListAdapter<ConsolidateWeatherItem, WeatherAdapter.ViewHolder>(DiffUtilCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = parent.inflate(R.layout.item_view_weather_day, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val weatherItem = getItem(position)
        holder.bind(weatherItem)
        holder.itemView.apply { setOnClickListener { listener(weatherItem) } }
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val binding: ItemViewWeatherDayBinding = ItemViewWeatherDayBinding.bind(view)
        fun bind(item: ConsolidateWeatherItem) = with(binding) {
            dayTextView.text = formatValue(item.applicableDate, DataTypes.DATE)
            temperatureTextView.text = formatValue(item.theTemp, DataTypes.TEMPERATURE)
            stateImage.loadUrl("https://www.metaweather.com/static/img/weather/png/${item.weatherStateAbbr}.png")
        }
    }
}

private object DiffUtilCallback : DiffUtil.ItemCallback<ConsolidateWeatherItem>() {
    override fun areItemsTheSame(
        oldItem: ConsolidateWeatherItem, newItem: ConsolidateWeatherItem,
    ): Boolean = oldItem.id == newItem.id

    override fun areContentsTheSame(
        oldItem: ConsolidateWeatherItem, newItem: ConsolidateWeatherItem,
    ): Boolean = oldItem == newItem
}