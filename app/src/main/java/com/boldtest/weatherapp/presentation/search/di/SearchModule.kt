package com.boldtest.weatherapp.presentation.search.di

import com.boldtest.weatherapp.data.repository.WeatherRepository
import com.boldtest.weatherapp.usecases.GetLocationsByQueryUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped

@Module
@InstallIn(ViewModelComponent::class)
class SearchModule {

    @Provides
    @ViewModelScoped
    fun provideGetLocationsByQuery(weatherRepository: WeatherRepository): GetLocationsByQueryUseCase =
        GetLocationsByQueryUseCase(weatherRepository)
}