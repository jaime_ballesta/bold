package com.boldtest.weatherapp.presentation.state

import com.boldtest.weatherapp.domain.model.LocationItem

sealed class SearchState {
    object LoadingState : SearchState()
    data class GoToHome(val titleLocation: String): SearchState()
    data class Error(val errorMessage: String) : SearchState()
    data class ShowLocations(val weatherReport: List<LocationItem>): SearchState()
}
