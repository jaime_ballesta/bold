package com.boldtest.weatherapp.presentation.home.viewmodel

import android.Manifest
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.boldtest.weatherapp.data.PermissionRequester
import com.boldtest.weatherapp.domain.Resource
import com.boldtest.weatherapp.domain.common.Errors.Companion.notFiltersErrorCode
import com.boldtest.weatherapp.domain.common.Errors.Companion.notLocationsErrorCode
import com.boldtest.weatherapp.domain.common.Errors.Companion.notWeatherStateErrorCode
import com.boldtest.weatherapp.domain.model.LocationItem
import com.boldtest.weatherapp.presentation.state.WeatherState
import com.boldtest.weatherapp.usecases.GetLocationsByCoordinatesUseCase
import com.boldtest.weatherapp.usecases.GetLocationsByQueryUseCase
import com.boldtest.weatherapp.usecases.GetWeatherReportUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class WeatherViewModel @Inject constructor(
    private val getLocationsByCoordinatesUseCase: GetLocationsByCoordinatesUseCase,
    private val getLocationsByQueryUseCase: GetLocationsByQueryUseCase,
    private val getWeatherReportUseCase: GetWeatherReportUseCase,
) : ViewModel() {

    private val _state = MutableStateFlow<WeatherState>(WeatherState.LoadingState)
    val state: StateFlow<WeatherState> = _state

    fun init() = viewModelScope.launch {
        setState(WeatherState.LoadingState)
        getLocationsByCoordinatesUseCase().collect { result ->
            when (result) {
                is Resource.Error -> setErrorState(result.message)
                is Resource.Loading -> setState(WeatherState.LoadingState)
                is Resource.Success -> result.data?.let { locations ->
                    successManagement(locations.first())
                    if (locations.isEmpty()) setErrorState(notLocationsErrorCode.toString())
                }
            }
        }
    }

    fun loadWeather(locationName: String) = viewModelScope.launch {
        setState(WeatherState.LoadingState)
        getLocationsByQueryUseCase(locationName).collect { result ->
            when (result) {
                is Resource.Error -> setErrorState(result.message)
                is Resource.Loading -> setState(WeatherState.LoadingState)
                is Resource.Success -> result.data?.let { locations ->
                    successManagement(locations.first())
                    if (locations.isEmpty()) setErrorState(notFiltersErrorCode.toString())
                }
            }
        }
    }

    fun Fragment.requestLocationPermission(afterRequest: (Boolean) -> Unit) {
        val locationPermissionRequester = PermissionRequester(
            this,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )
        viewModelScope.launch {
            val result = locationPermissionRequester.request()
            afterRequest(result)
        }
    }

    suspend fun successManagement(locationItem: LocationItem) {
        getWeatherReportUseCase(locationItem.woeId).collect { result ->
            when (result) {
                is Resource.Error -> setErrorState(result.message)
                is Resource.Loading -> setState(WeatherState.LoadingState)
                is Resource.Success -> result.data?.let {
                    setState(WeatherState.ShowWeatherConsolidation(it))
                } ?: setErrorState(notWeatherStateErrorCode.toString())
            }
        }
    }

    private fun setErrorState(errorCode: String?) {
        setState(WeatherState.Error(errorCode ?: "An unexpected error occured"))
    }

    private fun setState(state: WeatherState) {
        _state.value = state
    }

}