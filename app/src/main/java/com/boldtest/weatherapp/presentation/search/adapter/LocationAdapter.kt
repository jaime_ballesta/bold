package com.boldtest.weatherapp.presentation.search.adapter

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.boldtest.weatherapp.R
import com.boldtest.weatherapp.databinding.ItemViewLocationBinding
import com.boldtest.weatherapp.domain.model.LocationItem
import com.boldtest.weatherapp.inflate

class LocationAdapter(private val listener: (LocationItem) -> Unit) :
    ListAdapter<LocationItem, LocationAdapter.ViewHolder>(DiffUtilCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = parent.inflate(R.layout.item_view_location, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val locationItem = getItem(position)
        holder.bind(locationItem)
        holder.itemView.apply { setOnClickListener { listener(locationItem) } }
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val binding: ItemViewLocationBinding = ItemViewLocationBinding.bind(view)
        fun bind(item: LocationItem) = with(binding) {
            titleTextView.text = item.title
            locationTypeTextView.text = item.locationType
        }
    }
}

private object DiffUtilCallback : DiffUtil.ItemCallback<LocationItem>() {
    override fun areItemsTheSame(
        oldItem: LocationItem, newItem: LocationItem,
    ): Boolean = oldItem.woeId == newItem.woeId

    override fun areContentsTheSame(
        oldItem: LocationItem, newItem: LocationItem,
    ): Boolean = oldItem == newItem
}