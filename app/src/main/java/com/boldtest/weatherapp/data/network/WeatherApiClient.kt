package com.boldtest.weatherapp.data.network

import com.boldtest.weatherapp.data.model.LocationModel
import com.boldtest.weatherapp.data.response.WeatherResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface WeatherApiClient {

    @GET("location/search/")
    suspend fun getLocationsByCoordinates(
        @Query("lattlong") latLong: String,
    ): Response<List<LocationModel>>


    @GET("location/search/")
    suspend fun getLocationsByParam(
        @Query("query") query: String,
    ): Response<List<LocationModel>>


    @GET("location/{woeid}")
    suspend fun getWeatherReport(
        @Path("woeid") id: Int,
    ): Response<WeatherResponse>
}