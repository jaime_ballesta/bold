package com.boldtest.weatherapp.data.network

import com.boldtest.weatherapp.data.model.LocationModel
import com.boldtest.weatherapp.data.response.WeatherResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class WeatherService @Inject constructor(private val apiClient: WeatherApiClient) {

    suspend fun getLocationsByCoordinates(latLong: String): List<LocationModel> =
        withContext(Dispatchers.IO) {
            val response = apiClient.getLocationsByCoordinates(latLong)
            response.body() ?: emptyList()
        }

    suspend fun getLocationsByQuery(query: String): List<LocationModel> =
        withContext(Dispatchers.IO) {
            val response = apiClient.getLocationsByParam(query)
            response.body() ?: emptyList()
        }

    suspend fun getWeatherReport(id: Int): WeatherResponse = withContext(Dispatchers.IO) {
        val response = apiClient.getWeatherReport(id)
        response.body() ?: WeatherResponse()
    }
}