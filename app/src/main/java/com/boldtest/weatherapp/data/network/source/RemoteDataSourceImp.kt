package com.boldtest.weatherapp.data.network.source

import com.boldtest.weatherapp.data.mappers.toDomain
import com.boldtest.weatherapp.data.sources.RemoteDataSource
import com.boldtest.weatherapp.domain.model.LocationItem
import com.boldtest.weatherapp.domain.model.WeatherDetailsItem
import com.boldtest.weatherapp.data.network.WeatherService

class RemoteDataSourceImp(private val service: WeatherService) : RemoteDataSource {
    override suspend fun getLocationsByCoordinates(latLong: String): List<LocationItem> {
        val response = service.getLocationsByCoordinates(latLong)
        return response.map { it.toDomain() }
    }

    override suspend fun getLocationsByQuery(query: String): List<LocationItem> {
        val response = service.getLocationsByQuery(query)
        return response.map { it.toDomain() }
    }

    override suspend fun getWeatherReport(id: Int): WeatherDetailsItem {
        val response = service.getWeatherReport(id)
        return response.toDomain()
    }

}