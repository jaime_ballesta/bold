package com.boldtest.weatherapp

import android.text.SpannableStringBuilder
import android.text.style.RelativeSizeSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.activity.OnBackPressedCallback
import androidx.annotation.LayoutRes
import androidx.core.text.bold
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.boldtest.weatherapp.domain.common.DataTypes
import com.boldtest.weatherapp.domain.common.DataTypes.PERCENT
import com.boldtest.weatherapp.domain.common.DataTypes.SPEED
import com.boldtest.weatherapp.domain.common.DataTypes.TEMPERATURE
import com.boldtest.weatherapp.domain.common.DataTypes.DATE
import com.boldtest.weatherapp.domain.common.Errors.Companion.connectionErrorCode
import com.boldtest.weatherapp.domain.common.Errors.Companion.notFiltersErrorCode
import com.boldtest.weatherapp.domain.common.Errors.Companion.notLocationsErrorCode
import com.boldtest.weatherapp.domain.common.Errors.Companion.notWeatherStateErrorCode
import com.boldtest.weatherapp.domain.common.Errors.Companion.unknownErrorCode
import com.bumptech.glide.Glide
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.Locale


fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = true): View =
    LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)

fun ImageView.loadUrl(url: String) {
    Glide.with(context).load(url).into(this)
}

fun Fragment.getResourceView(resourceId: Int): View =
    requireActivity().findViewById(resourceId)

fun <T> LifecycleOwner.launchAndCollect(
    flow: Flow<T>,
    state: Lifecycle.State = Lifecycle.State.STARTED,
    body: (T) -> Unit,
) {
    lifecycleScope.launch {
        this@launchAndCollect.repeatOnLifecycle(state) {
            flow.collect(body)
        }
    }
}

fun Fragment.onBackPressedCustomAction(action: () -> Unit) {
    requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner,
        object : OnBackPressedCallback(true) {
            override
            fun handleOnBackPressed() {
                action()
            }
        })
}

fun <T> formatValue(value: T, type: DataTypes): String {
    val speedConverter = 1.6093440006147
    return when (type) {
        TEMPERATURE -> DecimalFormat("#ºC").format(value)
        PERCENT -> "${value}%"
        SPEED -> {
            val convertedValue = (value as Double) * speedConverter
            DecimalFormat("#,## km/h").format(convertedValue)
        }
        DATE -> {
            val date = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(value.toString())
            SimpleDateFormat("EEEE", Locale.getDefault()).format(date).replaceFirstChar {
                it.titlecase()
            }
        }
    }
}

fun Fragment.getErrorMessage(errorCode: String): String {
    return when (errorCode.toInt()) {
        connectionErrorCode -> getString(R.string.noConnectionMessage)
        notLocationsErrorCode -> getString(R.string.notLocationsMessage)
        notWeatherStateErrorCode -> getString(R.string.notWeatherStateMessage)
        notFiltersErrorCode -> getString(R.string.notFiltersMessage)
        unknownErrorCode -> getString(R.string.unknownMessage)
        else -> getString(R.string.unknownMessage)
    }
}

fun buildTitle(city: String, country: String): SpannableStringBuilder {
    val text = SpannableStringBuilder()
        .bold { append(city) }.append(", ")
        .append(country)
    text.setSpan(RelativeSizeSpan(.5f), (text.length - country.length), text.length, 0)
    return text
}

