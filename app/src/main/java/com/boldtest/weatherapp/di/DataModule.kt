package com.boldtest.weatherapp.di

import android.app.Application
import android.content.Context
import com.boldtest.weatherapp.data.PermissionChecker
import com.boldtest.weatherapp.data.PermissionCheckerImpl
import com.boldtest.weatherapp.data.repository.RegionRepository
import com.boldtest.weatherapp.data.repository.WeatherRepository
import com.boldtest.weatherapp.data.sources.LocationDataSource
import com.boldtest.weatherapp.data.sources.RemoteDataSource
import com.boldtest.weatherapp.data.network.WeatherService
import com.boldtest.weatherapp.data.network.source.LocationDataSourceImpl
import com.boldtest.weatherapp.data.network.source.RemoteDataSourceImp
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object DataModule {

    @Provides
    fun provideRemoteDataSource(weatherService: WeatherService): RemoteDataSource =
        RemoteDataSourceImp(weatherService)

    @Provides
    fun provideLocationDataSource(
        weatherService: WeatherService,
        @ApplicationContext context: Context,
    ): LocationDataSource =
        LocationDataSourceImpl(context as Application)

    @Provides
    fun providePermissionChecker(
        @ApplicationContext context: Context,
    ): PermissionChecker =
        PermissionCheckerImpl(context as Application)

    @Provides
    fun provideRepository(
        remoteDataSource: RemoteDataSource,
        regionRepository: RegionRepository,
    ): WeatherRepository =
        WeatherRepository(remoteDataSource, regionRepository)

    @Provides
    fun provideRegionRepository(
        locationDataSource: LocationDataSource,
        permissionChecker: PermissionChecker,
    ): RegionRepository =
        RegionRepository(locationDataSource, permissionChecker)

}