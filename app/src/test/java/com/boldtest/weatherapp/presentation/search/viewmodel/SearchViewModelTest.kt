package com.boldtest.weatherapp.presentation.search.viewmodel

import app.cash.turbine.test
import com.boldtest.weatherapp.domain.Resource
import com.boldtest.weatherapp.domain.common.Errors.Companion.notFiltersErrorCode
import com.boldtest.weatherapp.domain.model.LocationItem
import com.boldtest.weatherapp.presentation.CoroutinesTestRule
import com.boldtest.weatherapp.presentation.state.SearchState
import com.boldtest.weatherapp.usecases.GetLocationsByQueryUseCase
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class SearchViewModelTest {

    @get:Rule
    val coroutinesTestRule = CoroutinesTestRule()

    lateinit var searchViewModel: SearchViewModel

    @Mock
    private lateinit var getLocationsByQueryUseCase: GetLocationsByQueryUseCase

    private val locationMock = LocationItem(woeId = 123,
        distance = 20000,
        latLong = "123,567",
        locationType = "city",
        title = "Example")

    @Before
    fun setUp() {
        searchViewModel = SearchViewModel(getLocationsByQueryUseCase)
    }

    @Test
    fun `setSearchQuery listen Flows emits a location list from Api and update UI`() =
        coroutinesTestRule.testDispatcher.runBlockingTest {
            val resultExpected = Resource.Success(listOf(locationMock))

            whenever(getLocationsByQueryUseCase.invoke(locationMock.title)).thenReturn(flowOf(
                resultExpected))

            searchViewModel.setSearchQuery(locationMock.title)

            verify(getLocationsByQueryUseCase).invoke(locationMock.title)

            getLocationsByQueryUseCase.invoke(locationMock.title).collect {
                if (it is Resource.Success) {
                    Assert.assertEquals(resultExpected.data, it.data)
                }
            }
            searchViewModel.state.test {
                Assert.assertEquals(SearchState.ShowLocations(resultExpected.data!!),
                    expectMostRecentItem())
            }
        }

    @Test
    fun `setSearchQuery don't get results emits a empty list and update UI`() =
        coroutinesTestRule.testDispatcher.runBlockingTest {
            val resultExpected = Resource.Success<List<LocationItem>>(emptyList())

            whenever(getLocationsByQueryUseCase.invoke(locationMock.title)).thenReturn(flowOf(
                resultExpected))

            searchViewModel.setSearchQuery(locationMock.title)

            verify(getLocationsByQueryUseCase).invoke(locationMock.title)

            getLocationsByQueryUseCase.invoke(locationMock.title).collect {
                if (it is Resource.Success) {
                    Assert.assertEquals(resultExpected.data, it.data)
                }
            }
            searchViewModel.state.test {
                Assert.assertEquals(SearchState.Error(notFiltersErrorCode.toString()),
                    expectMostRecentItem())
            }
        }

    @Test
    fun `setSearchQuery get a empty param emits a empty list and update UI`() =
        coroutinesTestRule.testDispatcher.runBlockingTest {
            val resultExpected = Resource.Success<List<LocationItem>>(emptyList())

            searchViewModel.setSearchQuery(" ")

            searchViewModel.state.test {
                Assert.assertEquals(SearchState.ShowLocations(resultExpected.data!!),
                    expectMostRecentItem())
            }
        }

    @Test
    fun `locationSelected update UI and navigate to home`() =
        coroutinesTestRule.testDispatcher.runBlockingTest {
            val resultExpected = Resource.Success(locationMock)

            searchViewModel.locationSelected(locationMock)

            searchViewModel.state.test {
                Assert.assertEquals(SearchState.GoToHome(resultExpected.data!!.title),
                    expectMostRecentItem())
            }
        }


}