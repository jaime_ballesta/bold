package com.boldtest.weatherapp.presentation.home.viewmodel


import app.cash.turbine.test
import com.boldtest.weatherapp.domain.Resource
import com.boldtest.weatherapp.domain.model.ConsolidateWeatherItem
import com.boldtest.weatherapp.domain.model.LocationItem
import com.boldtest.weatherapp.domain.model.ParentItem
import com.boldtest.weatherapp.domain.model.WeatherDetailsItem
import com.boldtest.weatherapp.presentation.CoroutinesTestRule
import com.boldtest.weatherapp.presentation.state.WeatherState
import com.boldtest.weatherapp.usecases.GetLocationsByCoordinatesUseCase
import com.boldtest.weatherapp.usecases.GetLocationsByQueryUseCase
import com.boldtest.weatherapp.usecases.GetWeatherReportUseCase
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.any
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class WeatherViewModelTest {

    @get:Rule
    val coroutinesTestRule = CoroutinesTestRule()

    lateinit var weatherViewModel: WeatherViewModel

    @Mock
    private lateinit var getLocationsByQueryUseCase: GetLocationsByQueryUseCase

    @Mock
    private lateinit var getWeatherReportUseCase: GetWeatherReportUseCase

    @Mock
    private lateinit var getLocationsByCoordinatesUseCase: GetLocationsByCoordinatesUseCase

    private val locationMock = LocationItem(woeId = 123,
        distance = 20000,
        latLong = "123,567",
        locationType = "city",
        title = "Example")

    private val consolidateWeatherItem =
        ConsolidateWeatherItem(id = 123,
            applicableDate = "12-02-2022",
            humidity = 10,
            maxTemp = 22.0,
            minTemp = 9.0,
            predictability = 80,
            theTemp = 17.0,
            9.0,
            weatherStateAbbr = "he",
            weatherStateName = "Example",
            windSpeed = 22.0)

    private val weatherDetailsItem = WeatherDetailsItem(
        listOf(consolidateWeatherItem),
        latLong = "123,321",
        locationType = "city",
        parentItemModel = ParentItem(woeId = 1, locationType = "country", title = "Example"),
        sunRise = "x",
        sunSet = "y",
        title = "title",
        woeId = 111
    )

    @Before
    fun setUp() {
        weatherViewModel = WeatherViewModel(getLocationsByCoordinatesUseCase,
            getLocationsByQueryUseCase,
            getWeatherReportUseCase)
    }

    @Test
    fun `init listen Flows emits a location list from Api and call to getWeatherReportUseCase`() =
        coroutinesTestRule.testDispatcher.runBlockingTest {
            val resultExpected = Resource.Success(listOf(locationMock))

            whenever(getLocationsByCoordinatesUseCase.invoke()).thenReturn(flowOf(resultExpected))

            weatherViewModel.init()

            verify(getLocationsByCoordinatesUseCase).invoke()

            getLocationsByCoordinatesUseCase.invoke().collect {
                if (it is Resource.Success) {
                    Assert.assertEquals(resultExpected.data, it.data)
                }
            }

            verify(getWeatherReportUseCase).invoke(any())
        }

    @Test
    fun `init listen empty Flows emits the list of locations from Api and change the UI state`() =
        coroutinesTestRule.testDispatcher.runBlockingTest {
            val resultExpected = Resource.Success(emptyList<LocationItem>())

            whenever(getLocationsByCoordinatesUseCase.invoke()).thenReturn(flowOf(resultExpected))

            weatherViewModel.init()

            verify(getLocationsByCoordinatesUseCase).invoke()

            getLocationsByCoordinatesUseCase.invoke().collect {
                if (it is Resource.Success) {
                    Assert.assertEquals(resultExpected.data, it.data)
                }
            }

            weatherViewModel.state.test {
                Assert.assertEquals(WeatherState.LoadingState, expectMostRecentItem())
            }

        }

    @Test
    fun `loadWeather listen Flows emits a location list from Api and call to getWeatherReportUseCase`() =
        coroutinesTestRule.testDispatcher.runBlockingTest {
            val resultExpected = Resource.Success(listOf(locationMock))

            whenever(getLocationsByQueryUseCase.invoke(locationMock.title)).thenReturn(flowOf(
                resultExpected))

            weatherViewModel.loadWeather(locationMock.title)

            verify(getLocationsByQueryUseCase).invoke(locationMock.title)

            getLocationsByQueryUseCase.invoke(locationMock.title).collect {
                if (it is Resource.Success) {
                    Assert.assertEquals(resultExpected.data, it.data)
                }
            }
            verify(getWeatherReportUseCase).invoke(locationMock.woeId)
        }

    @Test
    fun `loadWeather listen empty Flows emits the list of locations from Api and change the UI state`() =
        coroutinesTestRule.testDispatcher.runBlockingTest {
            val resultExpected = Resource.Success(emptyList<LocationItem>())

            whenever(getLocationsByQueryUseCase.invoke(locationMock.title)).thenReturn(flowOf(
                resultExpected))

            weatherViewModel.loadWeather(locationMock.title)

            verify(getLocationsByQueryUseCase).invoke(locationMock.title)

            getLocationsByQueryUseCase.invoke(locationMock.title).collect {
                if (it is Resource.Success) {
                    Assert.assertEquals(resultExpected.data, it.data)
                }
            }

            weatherViewModel.state.test {
                Assert.assertEquals(WeatherState.LoadingState, expectMostRecentItem())
            }

        }

    @Test
    fun `successManagement listen Flows emits a weatherItem from Api and update UI state`() =
        coroutinesTestRule.testDispatcher.runBlockingTest {
            val resultExpected = Resource.Success(weatherDetailsItem)

            whenever(getWeatherReportUseCase.invoke(locationMock.woeId)).thenReturn(flowOf(
                resultExpected))

            weatherViewModel.successManagement(locationMock)

            verify(getWeatherReportUseCase).invoke(locationMock.woeId)

            getWeatherReportUseCase.invoke(locationMock.woeId).collect {
                if (it is Resource.Success) {
                    Assert.assertEquals(resultExpected.data, it.data)
                }
            }

            weatherViewModel.state.test {
                Assert.assertEquals(WeatherState.ShowWeatherConsolidation(resultExpected.data!!),
                    expectMostRecentItem())
            }
        }


}