package com.boldtest.weatherapp.usecases

import com.boldtest.weatherapp.data.repository.WeatherRepository
import com.boldtest.weatherapp.domain.Resource
import com.boldtest.weatherapp.domain.common.Errors.Companion.connectionErrorCode
import com.boldtest.weatherapp.domain.model.LocationItem
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException

class GetLocationsByCoordinatesUseCase(private val repository: WeatherRepository) {

    operator fun invoke(): Flow<Resource<List<LocationItem>>> = flow {
        try {
            emit(Resource.Loading())
            emit(Resource.Success(repository.getLocationsByCoordinates()))
        } catch (e: HttpException) {
            emit(Resource.Error(e.code().toString()))
        } catch (e: IOException) {
            emit(Resource.Error(connectionErrorCode.toString()))
        }
    }

}