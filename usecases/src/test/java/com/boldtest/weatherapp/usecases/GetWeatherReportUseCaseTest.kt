package com.boldtest.weatherapp.usecases

import com.boldtest.weatherapp.data.repository.WeatherRepository
import com.boldtest.weatherapp.domain.Resource
import com.boldtest.weatherapp.domain.model.ConsolidateWeatherItem
import com.boldtest.weatherapp.domain.model.ParentItem
import com.boldtest.weatherapp.domain.model.WeatherDetailsItem
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class GetWeatherReportUseCaseTest {

    @get:Rule
    val coroutinesTestRule = CoroutinesTestRule()

    @Mock
    lateinit var weatherRepository: WeatherRepository

    private lateinit var getWeatherReportUseCase: GetWeatherReportUseCase

    private val consolidateWeatherItem =
        ConsolidateWeatherItem(id = 123,
            applicableDate = "12-02-2022",
            humidity = 10,
            maxTemp = 22.0,
            minTemp = 9.0,
            predictability = 80,
            theTemp = 17.0,
            9.0,
            weatherStateAbbr = "he",
            weatherStateName = "Example",
            windSpeed = 22.0)

    private val weatherDetailsItem = WeatherDetailsItem(
        listOf(consolidateWeatherItem),
        latLong = "123,321",
        locationType = "city",
        parentItemModel = ParentItem(woeId = 1, locationType = "country", title = "Example"),
        sunRise = "x",
        sunSet = "y",
        title = "title",
        woeId = 111
    )

    @Before
    fun setUp() {
        getWeatherReportUseCase = GetWeatherReportUseCase(weatherRepository)
    }

    @Test
    fun `invoke must call weather consolidation from repository`() =
        coroutinesTestRule.testDispatcher.runBlockingTest {

            whenever(weatherRepository.getWeatherReport(weatherDetailsItem.woeId)).thenReturn(
                weatherDetailsItem)

            val result: Flow<Resource<WeatherDetailsItem>> =
                getWeatherReportUseCase.invoke(weatherDetailsItem.woeId)

            result.collect {
                if (it is Resource.Success) {
                    Assert.assertEquals(weatherDetailsItem, it.data)
                }
            }

        }

}