package com.boldtest.weatherapp.usecases

import com.boldtest.weatherapp.data.repository.WeatherRepository
import com.boldtest.weatherapp.domain.model.LocationItem
import com.boldtest.weatherapp.domain.Resource
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class GetLocationsByCoordinatesUseCaseTest {

    @get:Rule
    val coroutinesTestRule = CoroutinesTestRule()

    @Mock
    lateinit var weatherRepository: WeatherRepository

    private lateinit var getLocationsByCoordinates: GetLocationsByCoordinatesUseCase

    private val locationMock = listOf(
        LocationItem(woeId = 123,
            distance = 20000,
            latLong = "123,567",
            locationType = "city",
            title = "Example")
    )

    @Before
    fun setUp() {
        getLocationsByCoordinates = GetLocationsByCoordinatesUseCase(weatherRepository)
    }

    @Test
    fun `invoke must call locations from repository`() =
        coroutinesTestRule.testDispatcher.runBlockingTest {

            whenever(weatherRepository.getLocationsByCoordinates()).thenReturn(locationMock)

            val result: Flow<Resource<List<LocationItem>>> = getLocationsByCoordinates.invoke()

            result.collect {
                if (it is Resource.Success) {
                    Assert.assertEquals(locationMock, it.data)
                }
            }

        }

}