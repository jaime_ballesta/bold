package com.boldtest.weatherapp.data.model

import com.google.gson.annotations.SerializedName

data class ParentModel(
    @SerializedName("woeid") val woeId: Int = 0,
    @SerializedName("location_type") val locationType: String = "",
    @SerializedName("title") val title: String = "",
)