package com.boldtest.weatherapp.data.repository

import com.boldtest.weatherapp.data.sources.RemoteDataSource
import com.boldtest.weatherapp.domain.model.LocationItem
import com.boldtest.weatherapp.domain.model.WeatherDetailsItem

class WeatherRepository(
    private val remoteDataSource: RemoteDataSource,
    private val regionRepository: RegionRepository,
) {

    suspend fun getLocationsByCoordinates(): List<LocationItem> =
        remoteDataSource.getLocationsByCoordinates(regionRepository.findLastRegion())

    suspend fun getLocationsByQuery(query: String): List<LocationItem> =
        remoteDataSource.getLocationsByQuery(query)

    suspend fun getWeatherReport(id: Int): WeatherDetailsItem =
        remoteDataSource.getWeatherReport(id)
}