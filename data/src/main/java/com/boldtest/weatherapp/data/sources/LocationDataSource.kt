package com.boldtest.weatherapp.data.sources

interface LocationDataSource {
    suspend fun findLastRegion(): String?
}