package com.boldtest.weatherapp.data.model

import com.google.gson.annotations.SerializedName

data class ConsolidateWeatherModel(
    @SerializedName("id") val id: Long,
    @SerializedName("applicable_date") val applicableDate: String,
    @SerializedName("humidity") val humidity: Int,
    @SerializedName("max_temp") val maxTemp: Double,
    @SerializedName("min_temp") val minTemp: Double,
    @SerializedName("predictability") val predictability: Int,
    @SerializedName("the_temp") val theTemp: Double,
    @SerializedName("visibility") val visibility: Double,
    @SerializedName("weather_state_abbr") val weatherStateAbbr: String,
    @SerializedName("weather_state_name") val weatherStateName: String,
    @SerializedName("wind_speed") val windSpeed: Double,
)