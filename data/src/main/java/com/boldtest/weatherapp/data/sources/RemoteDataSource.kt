package com.boldtest.weatherapp.data.sources

import com.boldtest.weatherapp.domain.model.LocationItem
import com.boldtest.weatherapp.domain.model.WeatherDetailsItem

interface RemoteDataSource {
    suspend fun getLocationsByCoordinates(latLong: String): List<LocationItem>
    suspend fun getLocationsByQuery(query: String): List<LocationItem>
    suspend fun getWeatherReport(id: Int): WeatherDetailsItem
}