package com.boldtest.weatherapp.data.response

import com.boldtest.weatherapp.data.model.ConsolidateWeatherModel
import com.boldtest.weatherapp.data.model.ParentModel
import com.google.gson.annotations.SerializedName

data class WeatherResponse(
    @SerializedName("consolidated_weather")
    val consolidatedWeather: List<ConsolidateWeatherModel> = emptyList(),
    @SerializedName("latt_long") val latLong: String = "",
    @SerializedName("location_type") val locationType: String = "",
    @SerializedName("parent") val parentModel: ParentModel = ParentModel(),
    @SerializedName("sun_rise") val sunRise: String = "",
    @SerializedName("sun_set") val sunSet: String = "",
    @SerializedName("title") val title: String = "",
    @SerializedName("woeid") val woeId: Int = 0,
)