package com.boldtest.weatherapp.data.mappers

import com.boldtest.weatherapp.data.model.ConsolidateWeatherModel
import com.boldtest.weatherapp.data.model.LocationModel
import com.boldtest.weatherapp.data.model.ParentModel
import com.boldtest.weatherapp.data.response.WeatherResponse
import com.boldtest.weatherapp.domain.model.ConsolidateWeatherItem
import com.boldtest.weatherapp.domain.model.LocationItem
import com.boldtest.weatherapp.domain.model.ParentItem
import com.boldtest.weatherapp.domain.model.WeatherDetailsItem

fun LocationModel.toDomain(): LocationItem =
    LocationItem(woeId, distance, latLong, locationType, title)

fun WeatherResponse.toDomain(): WeatherDetailsItem =
    WeatherDetailsItem(consolidatedWeather.map { it.toDomain() },
        latLong,
        locationType,
        parentModel.toDomain(),
        sunRise,
        sunSet,
        title,
        woeId)

private fun ParentModel.toDomain(): ParentItem =
    ParentItem(woeId, locationType, title)

private fun ConsolidateWeatherModel.toDomain(): ConsolidateWeatherItem =
    ConsolidateWeatherItem(id,
        applicableDate,
        humidity,
        maxTemp,
        minTemp,
        predictability,
        theTemp,
        visibility,
        weatherStateAbbr,
        weatherStateName,
        windSpeed)