package com.boldtest.weatherapp.data.model

import com.google.gson.annotations.SerializedName

data class LocationModel(
    @SerializedName("woeid") val woeId: Int,
    @SerializedName("distance") val distance: Int,
    @SerializedName("latt_long") val latLong: String,
    @SerializedName("location_type") val locationType: String,
    @SerializedName("title") val title: String,
)