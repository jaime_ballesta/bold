package com.boldtest.weatherapp.domain.model

data class ConsolidateWeatherItem(
    val id: Long,
    val applicableDate: String,
    val humidity: Int,
    val maxTemp: Double,
    val minTemp: Double,
    val predictability: Int,
    val theTemp: Double,
    val visibility: Double,
    val weatherStateAbbr: String,
    val weatherStateName: String,
    val windSpeed: Double
)