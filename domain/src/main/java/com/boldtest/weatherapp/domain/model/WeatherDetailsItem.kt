package com.boldtest.weatherapp.domain.model

data class WeatherDetailsItem(
    val consolidatedWeather: List<ConsolidateWeatherItem>,
    val latLong: String,
    val locationType: String,
    val parentItemModel: ParentItem,
    val sunRise: String,
    val sunSet: String,
    val title: String,
    val woeId: Int,
)