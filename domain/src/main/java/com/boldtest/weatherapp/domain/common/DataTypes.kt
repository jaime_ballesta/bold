package com.boldtest.weatherapp.domain.common

enum class DataTypes {
    TEMPERATURE, PERCENT, SPEED, DATE
}