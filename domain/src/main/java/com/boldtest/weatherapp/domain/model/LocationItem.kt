package com.boldtest.weatherapp.domain.model


data class LocationItem(
    val woeId: Int,
    val distance: Int,
    val latLong: String,
    val locationType: String,
    val title: String
)