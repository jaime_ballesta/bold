package com.boldtest.weatherapp.domain.model

data class ParentItem(
    val woeId: Int,
    val locationType: String,
    val title: String,
)